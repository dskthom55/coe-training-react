import React from "react";
import {
  BrowserRouter,
  Route,
  RouteComponentProps,
  Switch
} from "react-router-dom";

export function App(): React.ReactElement<any> {
  return (
    <section>
      <BrowserRouter>
        <Switch>
          <Route
            path="/pokemon/:id"
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <div>I should be replaced with a component! :)</div>
            )}
          />
          <Route
            render={({ match }: RouteComponentProps<{ id: string }>) => (
              <div>I should be replaced with a component! :)</div>
            )}
          />
        </Switch>
      </BrowserRouter>
    </section>
  );
}
