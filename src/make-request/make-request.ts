import { useEffect, useState } from "react";
import { PokemonType } from "./poke-api.types";

const POKE_API_URL = "https://pokeapi.co/api/v2";

export async function get<T>(url: string): Promise<T> {
  const response = await fetch(url);
  return response.json();
}

export type PokemonResult = {
  pokemon?: PokemonType;
  error?: boolean;
};

export function useLoadPokemon(pokemonId: number): PokemonResult {
  const [pokemon, setPokemon] = useState<PokemonType>();
  const [error, setError] = useState<boolean>();

  const loadMove = async (): Promise<void> => {
    try {
      setPokemon(undefined);
      const url = `${POKE_API_URL}/pokemon/${pokemonId}`;
      setPokemon(await get<PokemonType>(url));
    } catch {
      setError(true);
    }
  };

  /* eslint-disable react-hooks/exhaustive-deps */
  useEffect(() => {
    loadMove();
  }, []);
  /* eslint-enable react-hooks/exhaustive-deps */

  return { pokemon, error };
}
